osmtogeojson '1944 eLSR.osm' > '1944 eLSR.geojson'
geojson-precision -p 6  '1944 eLSR.geojson' '1944 eLSR.geojson'
osmtogeojson '1944 ÖLSR.osm' > '1944 ÖLSR.geojson'
geojson-precision -p 6  '1944 ÖLSR.geojson' '1944 ÖLSR.geojson'
osmtogeojson '1945 Bombentrichter.osm' > '1945 Bombentrichter.geojson'
geojson-precision -p 6  '1945 Bombentrichter.geojson' '1945 Bombentrichter.geojson'
osmtogeojson '1945 Gebäude Leichte Schäden.osm' > '1945 Gebäude Leichte Schäden.geojson'
geojson-precision -p 6  '1945 Gebäude Leichte Schäden.geojson' '1945 Gebäude Leichte Schäden.geojson'
osmtogeojson '1945 Gebäude Schwere Schäden.osm' > '1945 Gebäude Schwere Schäden.geojson'
geojson-precision -p 6  '1945 Gebäude Schwere Schäden.geojson' '1945 Gebäude Schwere Schäden.geojson'
osmtogeojson '1945 Löschwasserbecken.osm' > '1945 Löschwasserbecken.geojson'
geojson-precision -p 6  '1945 Löschwasserbecken.geojson' '1945 Löschwasserbecken.geojson'