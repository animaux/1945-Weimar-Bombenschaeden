# Schadenskarten Weimar bis 1945

## Schäden 1945 aus Stadtarchiv Weimar 1-16-15

Schadensnangabe bei öffentlichen Gebäuden fehlen. Prozentzahlen vielleicht nur wegen Privaten Versicherungen?

Schwere und Totalschäden decken sich in Einzelfällen. Mittlere Schäden sind bei den schweren mit dabei.

## Problemstellen

+ Harthstraße 70, 63b und Hausnummern verl- Harthstraße
+ Kirschberg Krankenhaus und Kläranlage
+ Rießnerstraße, Adolf Welmar, 100% Garage v. Holz
+ Werkstattgebäude/Lagerraum, Ettersburger 5, Position unklar
+ Große Kromsdorfer Straße 15, 21 Position sehr vage vermutet, Behelfswohnungen?
+ Ernst-Abbe/Carl-Zeiss genaue Häuser unsicher
+ Dürrenbacher Hütte Hausnummern nicht zuzuordnen
+ Tiefurt ausgelassen
+ Brehmestraße 2a = Brennerstraße 2a (Ortskrankenkasse)
+ Kaufstraße 22 Scheune?
+ Marstallstraße/Marstall ohne Schadensnangabe
+ Hinter dem Bahnhof 10a, Lehrwerkstatt, Position unklar
+ Buttelstedter 2a, Bekleidungskammer, Position unklar
+ Hänselweg 64, Position unklar
+ Friesstr.[Kaumannstrasse] 9 Südenguth 5% " mit Lagerschuppen (unleserlich)
+ Frauentorstraße 17 (unleserlich)
+ Brennerstraße/Ebert 57 (?)/Meyer 32 (?) schwere Schäden (unleserlich)
+ Jakobskirchhof 12. Position unklar

## Quellen

Zusätzlich zu den oben genannten Titeln sind für die Schadens­karte folgende Quellen herangezogen worden:

+ OpenStreetMap-Mitwirkende (CC-BY-SA).
+ Anträge auf Entschädigung nach der Kriegsschädenverordnung vom 30. 11. 1940, Stadtarchiv Weimar 12 1-16-10.
+ Verzeichnis der Luftschutzräume (LSR) Weimar, unveröffentlichtes Manuskript von Klaus Mebus auf der Grundlage von Stadtarchiv Weimar 12 7-70-35.
+ Luftbilder aus den National Archives, Washington (USA), im Foto­archiv der Stiftung Gedenkstätten Buchenwald und Mittel­bau-Dora sowie historische Luftbilder in den offenen Daten des TLVermGeo (Thüringer Landesamt für Vermessung und Geoinformation)

## OSM —> GeoJSON

https://github.com/tyrasd/osmtogeojson

    osmtogeojson xyz.osm > xyz.geojson
    
## Reduce Precision

https://github.com/jczaplew/geojson-precision

    geojson-precision -p 6 input.json output.json
    
## GeoTIFF —> mbtiles

    /Library/Frameworks/GDAL.framework/Versions/2.2/Programs/gdal_translate file.tif file.mbtiles -of MBTILES